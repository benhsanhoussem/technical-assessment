import splitStrings from "../tasks/splitStrings/split-strings";

describe ('split strings', () => {
        const inputOutputArr = [
            {input: 'abc', output: ['ab', 'c_'], description: 'Split 3 characters String into pairs and add postfix'},
            {input: 'ab', output: ['ab'], description: 'put 2 chars string in array'},
            {input: 'abcd', output: ['ab', 'cd'], description: 'split 4 chars string to two parts'},
            {input: 'a', output: ['a_'], description: 'transform a char to array containing 2 chars string with postfix _'},
            {input: 'abcdef', output: ['ab', 'cd', 'ef'], description: 'Split 6 characters to three parts'},
            {input: '', output: [], description: 'return empty array if input string is empty'},
            {input: 'abcd_', output: ['ab', 'cd', '__'], description: 'should split string and NOT confuse _ char with _ postfix'},
        ];
        inputOutputArr.forEach(({input, output, description}) => {
            it(description, () => {
            const res = splitStrings(input);
            expect(res).toEqual(output);
        })
    })
})
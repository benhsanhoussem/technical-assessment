import conway from "../tasks/conwayLookAndSay/conway";

describe ('split strings', () => {
        const inputOutputArr = [
            {input: 11, output: 21, description: 'look and say of 11 should be 21'},
            {input: 3334, output: 3314, description: 'look and say of 3334 should be 3314'},
            {input: 1156666, output: 211546, description: 'look and say of 1156666 should be 211546'},
            {input: 21, output: 1211, description: 'look and say of 21 should be 1211'},
        ];
        inputOutputArr.forEach(({input, output, description}) => {
            it(description, () => {
            const res = conway(input);
            expect(res).toEqual(output);
        })
    })
})
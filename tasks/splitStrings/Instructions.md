# NOTES
Complete the solution so that it splits the string into pairs of two characters.
If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').

# Examples:

* 'abc' =>  ['ab', 'c_']
* 'abcdef' => ['ab', 'cd', 'ef']

# testing
* make sure your solution passes the unit tests in tests/split-strings.spec.js by running:
npm run test -- tests/split-strings.spec.js
in the project root folder